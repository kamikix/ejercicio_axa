package com.dchliferay.servletfilter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class PaginasServletFilter implements Filter {
	public void destroy() {
		logger.info("Cierro hook");
	}
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	//	String remoteAddress = request.getRemoteAddr();
		logger.info("<<página visitada>>");
		chain.doFilter(request, response);
	}
	public void init(FilterConfig config) throws ServletException {
		
		String intiParamValue = config.getInitParameter("initparam");
		logger.info("Inicio log intiParamValue: " + intiParamValue);
	
	}
	public PaginasServletFilter() {
		super();
	}
	private static final Log logger = LogFactoryUtil
			.getLog(PaginasServletFilter.class);
			
}
