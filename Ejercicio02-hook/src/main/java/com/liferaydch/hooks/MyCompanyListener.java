package com.liferaydch.hooks;


import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;


import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.Company;



public class MyCompanyListener extends BaseModelListener<Company> {
public void onBeforeCreate(Company company) throws ModelListenerException {

_log.info("Entramos antes de crear la company");

}

public void onBeforeRemove(Company company) throws ModelListenerException {
_log.info("Entramos antes de borrar la company");
}

public void onBeforeUpdate(Company company) throws ModelListenerException {
	_log.info("Entramos antes de modificar la company:"+company.getCompanyId());

	try {
		boolean casAuthEnabled = PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.CAS_AUTH_ENABLED);
		boolean casImportFromLdap = PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.CAS_IMPORT_FROM_LDAP);
		String casLoginURL = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.CAS_LOGIN_URL);
		String casLogoutURL = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.CAS_LOGOUT_URL);
		String casServerName = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.CAS_SERVER_NAME);
		String casServerURL = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.CAS_SERVER_URL);
		String casServiceURL = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.CAS_SERVICE_URL);
		String casNoSuchUserRedirectURL = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.CAS_NO_SUCH_USER_REDIRECT_URL);
		
		boolean ntlmAuthEnabled = PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.NTLM_AUTH_ENABLED);
		String ntlmDomainController = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.NTLM_DOMAIN_CONTROLLER);
		String ntlmDomainControllerName = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.NTLM_DOMAIN_CONTROLLER_NAME);
		String ntlmDomain = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.NTLM_DOMAIN);
		String ntlmServiceAccount = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.NTLM_SERVICE_ACCOUNT);
		String ntlmServicePassword = PrefsPropsUtil.getString(company.getCompanyId(), PropsKeys.NTLM_SERVICE_PASSWORD);

		
		_log.info("CAS_AUTH_ENABLED: "+casAuthEnabled);
		_log.info("casImportFromLdap: "+casImportFromLdap);
		_log.info("casLoginURL: "+casLoginURL);
		_log.info("casLogoutURL: "+casLogoutURL);
		_log.info("casServerName: "+casServerName);
		_log.info("casServerURL: "+casServerURL);
		_log.info("casServiceURL: "+casServiceURL);
		_log.info("casNoSuchUserRedirectURL: "+casNoSuchUserRedirectURL);
		
		_log.info("ntlmAuthEnabled: "+ntlmAuthEnabled);
		_log.info("ntlmDomainController: "+ntlmDomainController);
		_log.info("ntlmDomainControllerName: "+ntlmDomainControllerName);
		_log.info("ntlmDomain: "+ntlmDomain);
		_log.info("ntlmServiceAccount: "+ntlmServiceAccount);
		_log.info("ntlmServicePassword: "+ntlmServicePassword);
		
	
	} catch (SystemException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

private static Log _log =LogFactoryUtil.getLog(MyCompanyListener.class);
}