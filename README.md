** Realización de dos ejercicios pensando que el código resultante debería ir a producción.
a.	Hacer un desarrollo que saque una traza cada vez que se visite una página (por ejemplo <<página visitada>>) en los logs de Liferay.
b.	Hacer un desarrollo que saque una traza con los valores de los formularios cada vez que se modifique uno de los siguientes formularios. 
	Control Panel> Configuration > Instance Settings > Authentication > CAS 
	Control Panel> Configuration > Instance Settings > Authentication > NTLM